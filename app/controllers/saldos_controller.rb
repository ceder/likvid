class SaldosController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @saldo_pages, @saldos = paginate :saldos, :per_page => 10,
     :order => "date desc, account_id"
  end

  def show
    @saldo = Saldo.find(params[:id])
  end

  def new
    @saldo = Saldo.new
  end

  def create
    @saldo = Saldo.new(params[:saldo])
    if @saldo.save
      flash[:notice] = 'Saldo was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @saldo = Saldo.find(params[:id])
  end

  def update
    @saldo = Saldo.find(params[:id])
    if @saldo.update_attributes(params[:saldo])
      flash[:notice] = 'Saldo was successfully updated.'
      redirect_to :action => 'show', :id => @saldo
    else
      render :action => 'edit'
    end
  end

  def destroy
    Saldo.find(params[:id]).destroy
    redirect_to :action => 'list'
  end

  def new_snapshot
    @saldos = []
    for account in Account.find_all
        @saldos << Saldo.new(:account_id => account.id,
		   	     :date => Date.today)
    end
  end

  def create_snapshot
    ok = true
    @saldos = []
    (0...params[:saldos].to_i).each { |ix|
      if params[:saldo][ix.to_s][:amount] != ""
        saldo = Saldo.new(params[:saldo][ix.to_s])
        puts ix
        puts params[:saldo][ix.to_s]
        puts params[:saldo]
        if not saldo.save
          ok = false
        end
        @saldos << saldo
      end
    }
    if ok
      flash[:notice] = 'Saldos were successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new_snapshot'
    end
  end
end
