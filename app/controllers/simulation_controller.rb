# -*- coding: utf-8 -*-
class SimulationController < ApplicationController

  def simulate
    all_saldos = Saldo.find_by_sql(
        "SELECT saldos.id, saldos.amount, saldos.date, saldos.account_id FROM" +
	" (SELECT account_id, MAX(date) AS maxdate" +
	"  FROM saldos GROUP BY account_id) AS maxx, saldos" +
	" WHERE maxx.account_id = saldos.account_id" +
	" AND maxx.maxdate = saldos.date" +
	" ORDER BY id")

    @start_amount = 0
    @start_date = nil
    @start_saldos = []
    for saldo in all_saldos
      if saldo.account.shortterm == true
        @start_amount += saldo.amount
	@start_saldos << saldo
        if @start_date.nil? or @start_date < saldo.date
      	   @start_date = saldo.date
        end
      end
    end
	
    items = RecurringItemRange.find(:all,
      :conditions => ["enddate > ? OR enddate IS NULL", @start_date],
      :include => [:recurring_item, :schedule])

    items.each { |item|
      item.set_simulation_date(@start_date)
    }

    if false
      items.sort!
      
      items.each { |item|
        puts item.recurring_item.description, item.next_event, item.amount
      }
    end

    now = Time.utc(@start_date.year, @start_date.month, @start_date.day)
    @start_time = now

    if not params[:enddate].nil?
      @end_time = Time.parse(params[:enddate])
    else
      start = Time.utc(@start_date.year, @start_date.month, @start_date.day)
      @end_time = now.advance(:months => if params[:months].nil?
      	       	 		         then 3
					 else params[:months].to_i
					 end)
    end

    @end_date = Date.civil(@end_time.year, @end_time.month, @end_time.day)

    amount = @start_amount
    @min_amount = amount
    @max_amount = amount

    @actions = [[time_to_date(now), "Ingående saldo", 0, amount]]

    while now <= @end_time
      items.sort!
      i = items[0]
      if i.next_event.nil?
        break
      end
      change = i.next_amount
      amount += change
      entry = [i.next_date, i.recurring_item.description, change, amount]
      @actions << entry

      if @min_amount > amount
        @min_amount = amount
      end

      if @max_amount < amount
        @max_amount = amount
      end

      now = i.next_event
      i.forward
    end

  end

  def show
    simulate
    g = Gruff::Line.new
    # Uncomment to use your own theme or font
    # See http://colourlovers.com or http://www.firewheeldesign.com/widgets/ for color ideas
#     g.theme = {
#       :colors => ['#663366', '#cccc99', '#cc6633', '#cc9966', '#99cc99'],
#       :marker_color => 'white',
#       :background_colors => ['black', '#333333']
#     }
#     g.font = File.expand_path('artwork/fonts/VeraBd.ttf', RAILS_ROOT)

    g.title = "Simulation"
    
    datapoints = []
    labels = {}
    @actions.each { |data|
      date = data[0]
      amount = data[3]

      if date.day == 1
        labels[datapoints.size] = "#{date.year}-#{date.month}"
      end
      datapoints << amount
    }
     
    g.data("Cash", datapoints)

    g.labels = labels

    send_data(g.to_blob, :disposition => 'inline', :type => 'image/png', :filename => "gruff.png")
  end

  def time_to_date(t)
    Date.civil(t.year, t.month, t.day)
  end

  def date_to_time(d)
    Time.utc(d.year, d.month, d.day)
  end

end
