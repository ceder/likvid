class RecurringItemRangesController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @recurring_item_range_pages, @recurring_item_ranges = paginate(
     :recurring_item_ranges, :per_page => 10,
     :order => "recurring_item_id, startdate")
  end

  def show
    @recurring_item_range = RecurringItemRange.find(params[:id])
  end

  def new
    @recurring_item_range = RecurringItemRange.new
  end

  def create
    @recurring_item_range = RecurringItemRange.new(params[:recurring_item_range])
    if @recurring_item_range.save
      flash[:notice] = 'RecurringItemRange was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @recurring_item_range = RecurringItemRange.find(params[:id])
  end

  def update
    @recurring_item_range = RecurringItemRange.find(params[:id])
    if @recurring_item_range.update_attributes(params[:recurring_item_range])
      flash[:notice] = 'RecurringItemRange was successfully updated.'
      redirect_to :action => 'show', :id => @recurring_item_range
    else
      render :action => 'edit'
    end
  end

  def destroy
    RecurringItemRange.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
