class RecurringItemsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @recurring_item_pages, @recurring_items = paginate :recurring_items, :per_page => 10
  end

  def show
    @recurring_item = RecurringItem.find(params[:id])
  end

  def new
    @recurring_item = RecurringItem.new
  end

  def create
    @recurring_item = RecurringItem.new(params[:recurring_item])
    if @recurring_item.save
      flash[:notice] = 'RecurringItem was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @recurring_item = RecurringItem.find(params[:id])
  end

  def update
    @recurring_item = RecurringItem.find(params[:id])
    if @recurring_item.update_attributes(params[:recurring_item])
      flash[:notice] = 'RecurringItem was successfully updated.'
      redirect_to :action => 'show', :id => @recurring_item
    else
      render :action => 'edit'
    end
  end

  def destroy
    RecurringItem.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
