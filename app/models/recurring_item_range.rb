class RecurringItemRange < ActiveRecord::Base
  belongs_to :schedule
  belongs_to :recurring_item

  def set_simulation_date(date)
    @simulation_date = date
    @simulation_time = date_to_time(date)
    if self.spread_out
      @next_event = date_to_time(date)
    else
      @next_event = date_to_time(self.startdate)
    end
      
    # FIXME: This can probably be done more efficiently, by finding
    # out how many months we need to forward.
    while (not @next_event.nil?) and @next_event < @simulation_time
      self.forward
    end
  end

  def <=>(other)
    if @next_event.nil? and other.next_event.nil?
      return 0
    elsif @next_event.nil?
      return 1
    elsif other.next_event.nil?
      return -1
    elsif @next_event != other.next_event
      return @next_event <=> other.next_event
    else
      return -(amount <=> other.amount)
    end
  end

  def next_event
    return @next_event
  end

  def next_amount
    if self.spread_out
      this_month = Time.utc(@next_event.year, @next_event.month, 1)
      next_month = this_month.advance(:months => 1)
      days_in_month = time_to_date(next_month) - time_to_date(this_month)

      month_amount = self.amount.to_f

      yesterday = ((month_amount * (@next_event.day - 1)) / days_in_month).to_i
      today = ((month_amount * @next_event.day) / days_in_month).to_i

      return today - yesterday
    else
      return self.amount
    end
  end

  def time_to_date(t)
    Date.civil(t.year, t.month, t.day)
  end

  def date_to_time(d)
    Time.utc(d.year, d.month, d.day)
  end

  def next_date
    time_to_date(@next_event)
  end

  def forward
    if not @next_event.nil?
      if self.spread_out
        @next_event = @next_event.advance(:days => 1)
      else
        @next_event = @next_event.advance(:months => 
		      			  self.schedule.monthly_repeat)
      end
      if not self.enddate.nil? and @next_event > self.endtime
        @next_event = nil
      end
    end
  end

  def endtime
    date_to_time(self.enddate)
  end

end
