# -*- coding: utf-8 -*-
class CreateSaldos < ActiveRecord::Migration
  def self.up
    create_table (:saldos,
		  :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8') do |t|
      t.column :amount, :integer
      t.column :date, :date
      t.column :account_id, :integer
    end

    if RAILS_ENV == "development"
      ppay = Account.find_by_name("Pers lönekonto").id
      pvisa = Account.find_by_name("Pers VISA-kort").id
      ptrans = Account.find_by_name("Pers transkonto").id
      ica = Account.find_by_name("ICA kundkort").id
      kpay = Account.find_by_name("Kristinas lönekonto").id
      pfund = Account.find_by_name("Pers aktiefond (Banco)").id
      psaved = Account.find_by_name("Pers kapitalförsäkring").id
      
      date1 = Date.civil(2006, 8, 10)
      date2 = Date.civil(2006, 8, 14)
      date3 = Date.civil(2006, 8, 18)
      
      Saldo.create :amount => 1231, :date => date1, :account_id => ppay
      Saldo.create :amount => 4000, :date => date2, :account_id => ppay
      Saldo.create :amount => 5000, :date => date1, :account_id => pvisa
      Saldo.create :amount => 4500, :date => date2, :account_id => pvisa
      Saldo.create :amount => 5, :date => date1, :account_id => ptrans
      Saldo.create :amount => 5, :date => date2, :account_id => ptrans
      Saldo.create :amount => 4000, :date => date1, :account_id => ica
      Saldo.create :amount => 2000, :date => date2, :account_id => ica
      Saldo.create :amount => 19000, :date => date1, :account_id => kpay
      Saldo.create :amount => 17000, :date => date2, :account_id => kpay
      Saldo.create :amount => 23000, :date => date1, :account_id => pfund
      Saldo.create :amount => 23000, :date => date2, :account_id => pfund
      Saldo.create :amount => 18000, :date => date1, :account_id => psaved
      Saldo.create :amount => 18000, :date => date2, :account_id => psaved
      Saldo.create :amount => 18020, :date => date3, :account_id => psaved
    end

  end

  def self.down
    drop_table :saldos
  end
end
