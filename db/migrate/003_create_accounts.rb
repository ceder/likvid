# -*- coding: utf-8 -*-
class CreateAccounts < ActiveRecord::Migration
  def self.up
    create_table (:accounts,
		  :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8') do |t|
      t.column :name, :string
      t.column :shortterm, :boolean
    end

    if RAILS_ENV == "development"
      Account.create :name => "Pers lönekonto", :shortterm => true
      Account.create :name => "Pers VISA-kort", :shortterm => true
      Account.create :name => "Pers transkonto", :shortterm => true
      Account.create :name => "ICA kundkort", :shortterm => true
      Account.create :name => "Kristinas lönekonto", :shortterm => true
      Account.create :name => "Pers aktiefond (Banco)", :shortterm => false
      Account.create :name => "Pers kapitalförsäkring", :shortterm => false
    end

  end

  def self.down
    drop_table :accounts
  end
end
