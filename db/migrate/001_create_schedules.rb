# -*- coding: utf-8 -*-
class CreateSchedules < ActiveRecord::Migration
  def self.up
    create_table (:schedules,
		  :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8') do |t|
      t.column :name, :string
      t.column :monthly_repeat, :integer

      t.column :created_on, :timestamp
      t.column :updated_on, :timestamp
    end

    if RAILS_ENV == "development"
      Schedule.create :name => "Månad", :monthly_repeat => 1
      Schedule.create :name => "Kvartal", :monthly_repeat => 3
      Schedule.create :name => "År", :monthly_repeat => 12
    end

  end

  def self.down
    drop_table :schedules
  end
end
