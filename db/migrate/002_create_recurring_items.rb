# -*- coding: utf-8 -*-
class CreateRecurringItems < ActiveRecord::Migration
  def self.up
    create_table (:recurring_items,
		  :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8') do |t|
      t.column :description, :string

      t.column :created_on, :timestamp
      t.column :updated_on, :timestamp
    end

    if RAILS_ENV == "development"
      RecurringItem.create :description => "Pers lön"
      RecurringItem.create :description => "Pers lön 10"
      RecurringItem.create :description => "Telia"
      RecurringItem.create :description => "Mat"
    end

  end

  def self.down
    drop_table :recurring_items
  end
end
