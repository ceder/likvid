# -*- coding: utf-8 -*-
class CreateRecurringItemRanges < ActiveRecord::Migration
  def self.up
    create_table :recurring_item_ranges do |t|
      t.column :recurring_item_id, :integer

      t.column :amount, :integer
      t.column :startdate, :date
      t.column :enddate, :date
      t.column :schedule_id, :integer
      t.column :spread_out, :boolean
    end

    if RAILS_ENV == "development"
      pay = RecurringItem.find_by_description("Pers lön").id
      pay10 = RecurringItem.find_by_description("Pers lön 10").id
      telia = RecurringItem.find_by_description("Telia").id
      food = RecurringItem.find_by_description("Mat").id
      
      month = Schedule.find_by_name("Månad").id
      quarter = Schedule.find_by_name("Kvartal").id
      
      RecurringItemRange.create(:recurring_item_id => pay,
      			        :amount => "22200",
				:startdate => Date.civil(2006, 2, 25),
				:enddate => Date.civil(2006, 10, 24),
				:schedule_id => month,
				:spread_out => false)
      
      RecurringItemRange.create(:recurring_item_id => pay10,
      			        :amount => "-3700",
				:startdate => Date.civil(2006, 8, 25),
				:enddate => Date.civil(2006, 10, 24),
				:schedule_id => month,
				:spread_out => false)
      
      RecurringItemRange.create(:recurring_item_id => pay,
      			        :amount => "23200",
				:startdate => Date.civil(2006, 10, 25),
				:schedule_id => month,
				:spread_out => false)
      
      RecurringItemRange.create(:recurring_item_id => pay10,
      			        :amount => "-3900",
				:startdate => Date.civil(2006, 10, 25),
				:schedule_id => month,
				:spread_out => false)
      
      RecurringItemRange.create(:recurring_item_id => telia,
      			        :amount => "-1200",
				:startdate => Date.civil(2006, 9, 10),
				:schedule_id => quarter,
				:spread_out => false)

      RecurringItemRange.create(:recurring_item_id => food,
      			        :amount => "-4000",
				:startdate => Date.civil(2006, 7, 25),
				:schedule_id => month,
				:spread_out => true)
    end
  end

  def self.down
    drop_table :recurring_item_ranges
  end
end
