require File.dirname(__FILE__) + '/../test_helper'
require 'recurring_item_ranges_controller'

# Re-raise errors caught by the controller.
class RecurringItemRangesController; def rescue_action(e) raise e end; end

class RecurringItemRangesControllerTest < Test::Unit::TestCase
  fixtures :recurring_item_ranges

  def setup
    @controller = RecurringItemRangesController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:recurring_item_ranges)
  end

  def test_show
    get :show, :id => 1

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:recurring_item_range)
    assert assigns(:recurring_item_range).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:recurring_item_range)
  end

  def test_create
    num_recurring_item_ranges = RecurringItemRange.count

    post :create, :recurring_item_range => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_recurring_item_ranges + 1, RecurringItemRange.count
  end

  def test_edit
    get :edit, :id => 1

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:recurring_item_range)
    assert assigns(:recurring_item_range).valid?
  end

  def test_update
    post :update, :id => 1
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => 1
  end

  def test_destroy
    assert_not_nil RecurringItemRange.find(1)

    post :destroy, :id => 1
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      RecurringItemRange.find(1)
    }
  end
end
