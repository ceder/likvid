require File.dirname(__FILE__) + '/../test_helper'
require 'recurring_items_controller'

# Re-raise errors caught by the controller.
class RecurringItemsController; def rescue_action(e) raise e end; end

class RecurringItemsControllerTest < Test::Unit::TestCase
  fixtures :recurring_items

  def setup
    @controller = RecurringItemsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:recurring_items)
  end

  def test_show
    get :show, :id => 1

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:recurring_items)
    assert assigns(:recurring_items).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:recurring_items)
  end

  def test_create
    num_recurring_items = RecurringItems.count

    post :create, :recurring_items => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_recurring_items + 1, RecurringItems.count
  end

  def test_edit
    get :edit, :id => 1

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:recurring_items)
    assert assigns(:recurring_items).valid?
  end

  def test_update
    post :update, :id => 1
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => 1
  end

  def test_destroy
    assert_not_nil RecurringItems.find(1)

    post :destroy, :id => 1
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      RecurringItems.find(1)
    }
  end
end
